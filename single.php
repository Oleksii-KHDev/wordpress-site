<?php
get_header();
?>
<main class="article-details-page">
    <div class="container position-relative">
        <span class="bg-letter bg-r d-none d-lg-flex"><?= get_field('background_letter_1') ? get_field('background_letter_1') : get_field('watermark_1', 'options') ?></span>
        <span class="bg-letter bg-d d-none d-lg-flex"><?= get_field('background_letter_2') ? get_field('background_letter_2') : get_field('watermark_2', 'options') ?></span>
        <span class="bg-letter bg-text d-none d-lg-flex"><?= get_field('background_text') ? get_field('background_text') : get_field('watermark_3', 'options') ?></span>
        <h1 class="main-heading"><?php the_title() ?></h1>
        <img src="<?= get_the_post_thumbnail_url(null, 'full') ?>" class="main-img" alt="">
        <div class="row pb-5">

            <div class="col-12 col-lg-10 center-block">
                
		<h2 class="article-heading"><?= get_the_excerpt() ?></h2>
                <p><?= do_shortcode( get_the_content() ) ?></p>
            </div>
        </div>
    </div>
</main>
<?php
get_footer();