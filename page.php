<?php
get_header();
?>
<main class="article-details-page">
    <div class="container position-relative">
        <h1 class="main-heading"><?php the_title() ?></h1>
        <div class="content-wrapper"
            <div class="row pb-5">
                <div class="col-12 col-lg-10 center-block">
    <!--                <h2 class="article-heading">--><?php //the_excerpt() ?><!--</h2>-->
                    <p><?php the_content() ?></p>
                </div>
            </div>
        </div>
    </div>
</main>
<?php
get_footer();