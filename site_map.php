<?php
/**
 * Template Name: SiteMap
 * Template Post Type: page
 */
get_header();
?>

<div class="container">
	<h1 class="h1"><?php the_title(); ?></h1>
	<?php
	do_shortcode(the_content());
	// do_shortcode('[wp_sitemap_page]');
	?>
</div>
<?php
get_footer();

