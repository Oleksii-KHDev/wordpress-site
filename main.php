<?php
/**
 * Template Name: Main
 * Template Post Type: page
 */
get_header();

?>
    <main class="home">
        <section class="header-bottom">
            <div id="carouselHeaderIndicators" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselHeaderIndicators" data-bs-slide-to="0" class="active"
                            aria-current="true" aria-label="Slide 1"><span></span></button>
                    <button type="button" data-bs-target="#carouselHeaderIndicators" data-bs-slide-to="1"
                            aria-label="Slide 2"><span></span></button>
                    <button type="button" data-bs-target="#carouselHeaderIndicators" data-bs-slide-to="2"
                            aria-label="Slide 3"><span></span></button>
                </div>
                <div class="carousel-inner">
					<?php if ( have_rows( 'slides' ) ):
					$i = 1; ?>
					<?php while ( have_rows( 'slides' ) ):
					the_row(); ?>
					<?php if ( $i == 1 ) : ?>
                    <div class="carousel-item active">
						<?php else : ?>
                        <div class="carousel-item">
							<?php endif; ?> <!-- if ($i == 1) -->
                            <img src="<?php the_sub_field( 'slide_image' ); ?>" class="" alt="">
                            <div
                                    class="container h-100 d-flex flex-column justify-content-center align-items-lg-center align-items-lg-start">
                                <div class="item-content">

                                    <div style="color: <?php the_sub_field( 'slide_color_text' ) ?>;" class="c-white text-center text-lg-start h1 div-h1"><?php the_sub_field( 'slide_title' ); ?></div>
                                    <p style="color: <?php the_sub_field( 'slide_color_text' ) ?>;" class="c-white text-center text-lg-start"><?php the_sub_field( 'slide_text' ); ?></p>
                                </div>
                            </div>
                        </div>
						<?php $i ++; ?>
						<?php endwhile; ?> <!-- while( have_rows('slides') ) -->
						<?php endif; ?> <!-- if( have_rows('slides') ) -->
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselHeaderIndicators"
                            data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselHeaderIndicators"
                            data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                </div>
        </section>
        <?php $posts = get_posts( array(
                            'numberposts' => 4,
                            'orderby'     => 'date',
                            'order'       => 'DESC',
                            'suppress_filters' => true,
                        ) );
        ?>
        <section class="articles">
		    <?php if( get_field( 'articles_block_title' ) ) : ?><div class="custom-border-top h3 div-h3"><?= get_field( 'articles_block_title' ) ?></div><?php endif; ?>
		    <?php if( get_field( 'title_caption' ) ) : ?><p><?= get_field( 'title_caption' ) ?></p><?php endif; ?>

            <div id="carouselArticlesIndicators" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">

                        <button class="arrow d-none d-lg-flex" type="button" data-bs-target="#carouselArticlesIndicators"
                                data-bs-slide="prev">
                            <span class="arrow-left" aria-hidden="true"></span>
                        </button>
                        <button type="button" data-bs-target="#carouselArticlesIndicators" data-bs-slide-to="0"
                                    class="active" aria-current="true" aria-label="Slide 1"><span></span></button>
                        <button type="button" data-bs-target="#carouselArticlesIndicators" data-bs-slide-to="1"
                             aria-current="true" aria-label="Slide 2"><span></span></button>
                        <button type="button" data-bs-target="#carouselArticlesIndicators" data-bs-slide-to="2"
                             aria-current="true" aria-label="Slide 3"><span></span></button>
                        <button type="button" data-bs-target="#carouselArticlesIndicators" data-bs-slide-to="3"
                             aria-current="true" aria-label="Slide 4"><span></span></button>
                        <button class="arrow d-none d-lg-block" type="button" data-bs-target="#carouselArticlesIndicators"
                                data-bs-slide="next">
                            <span class="arrow-right" aria-hidden="true"></span>
                        </button>
                </div>
                <div class="carousel-inner">
				    <?php $i = 1; ?>
				    <?php foreach ($posts as $article): ?>


				    <?php if ( $i == 1 ) : ?>
                    <div class="carousel-item active" data-article-link = "<?= get_permalink($article) ?>" >
					    <?php else: ?>
                        <div class="carousel-item" data-article-link = "<?= get_permalink($article) ?>" >
						    <?php endif; ?>
						    <?php $i ++; ?>
                            <a href="<?= get_permalink($article) ?>">
                                <img src="<?= get_the_post_thumbnail_url( $article, 'full' ); ?>" class="" alt=""></a>
                            <div class="item-content" onclick="window.location = '<?= get_permalink($article) ?>'">
                                <div class="c-white div-h3 h3"><?= $article->post_title ?></div>
                                <p class="c-white"><?= $article->post_excerpt ?></p>
                                <a href="<?= get_permalink( get_option( 'page_for_posts' ) ) ?>" class="custom-btn d-lg-none"><?= get_field( 'right_button_text' ) ?></a>
                                <a href="<?= get_permalink( $article ) ?>"
                                   class="d-none d-lg-block text-decoration-underline"><?= get_field( 'link_on_article' ) ?>
                                </a>
                            </div>


                        </div>
					<?php endforeach; ?> <!-- foreach ($rows as $row): ?> -->
                    </div>

                </div>
                <div class="btn-box w-100 justify-content-center d-none d-lg-flex">
                    <a href="<?= get_permalink( get_option( 'page_for_posts' ) ) ?>"
                       class="custom-btn"><?= __('All articles', 'gold-bull') ?></a>
                </div>
			    <?php $upLoadDir = wp_get_upload_dir() ?>
                <div class="alert alert-warning center-block">
                    <div style="color:red;padding-top: 20px;">Не пропустите новости из мира инвестиций в нашем телеграмм канале</div>
                    <div style="padding-top: 20px;"><img style="width: 70px; height: 70px; border-radius: 35px;" src="<?= $upLoadDir['baseurl'] . '/Investor-Plus-logo.jpg'?>"><a style="color:blue; text-decoration:underline; margin-left: 10px;" href="https://t.me/gold_bull_invest">Инвестор ПЛЮС</a></div>
                </div>
        </section>

        <section id="cycle" class="super-cycle">
            <div class="custom-border-top h3"><?= get_field( 'cycle_block_title' ) ?></div>
            <div class="super-cycle-content text-center">
                <div class="container">
                    <?php $cycle_text = get_field( 'cycle_text' ); ?>
                    <?php $cycle_text = preg_replace('/align="justify"/', 'style="text-align:justify;"', $cycle_text ); ?>
                    <div id="textCycle" class="text-prom-cycle c-white"><?= $cycle_text  ?></div>
                    <a href="" id="moreCycle" class="more-cycle"><?= get_field( 'cycle_left_button_text' ) ?></a>
                    <button type="button" class="custom-btn c-white mt-2 center-block" data-bs-toggle="modal"
                            data-bs-target="<?= get_field( 'cycle_right_button_url' ) ?>"><?= get_field( 'cycle_right_button_text' ) ?></button>
                </div>
            </div>
        </section>
        <section id="promotions" class="promotions">
            <div class="container">
                <h1 class="custom-border-top text-center h3 div-h3"><?= get_field( 'share_block_title' ) ?></h1>
                <?php $share_title_caption = get_field( 'share_title_caption' ); ?>
                <?php $share_title_caption = preg_replace('/align="justify"/', 'style="text-align:justify;"', $share_title_caption ); ?>
                <div class="center-block p"><?= $share_title_caption ?></div>
                <div class="row justify-content-lg-between justify-content-lg-around">
					<?php if ( have_rows( 'share_category' ) ): ?>
						<?php while ( have_rows( 'share_category' ) ): the_row(); ?>
                            <div class="col-12 col-md-6 col-lg-4 content-box">
                                <img src="<?= the_sub_field( 'category_image' ) ?>" alt="" class="w-100 more">
                                <div class="div-h2 h2"><?= the_sub_field( 'category_title' ) ?></div>
                                <p class="text-prom"><?= the_sub_field( 'category_text' ) ?></p>
                                <a href="<?= the_sub_field( 'category_url' ) ?>"
                                   class="more"><?= the_sub_field( 'category_url_text' ) ?></a>
                            </div>
						<?php endwhile; ?> <!-- while( have_rows('articles') ) -->
					<?php endif; ?> <!-- if( have_rows('articles') ) -->
                </div>
                <button type="button" class="signin d-none d-lg-flex custom-btn center-block c-white"
                        data-bs-toggle="modal"
                        data-bs-target="<?= get_field( 'button_url' ) ?>"><?= get_field( 'button_text' ) ?></button>
            </div>
        </section>
        <section id="about" class="about-company">
            <div class="container d-lg-flex justify-content-lg-around">
                <div class="row">
                    <div class="col-12 col-lg-6 col-xxl-4">
                        <div class="about-content">
                            <h3 class="custom-border-top h4 div-h4"><?= get_field( 'about_company_block_title' ) ?></h3>
                            <div class="div-h2 h2"><?= get_field( 'company_name' ) ?></div>
                            <?php $about_company_block_text = get_field( 'about_company_block_text' ); ?>
                            <?php $about_company_block_text = preg_replace('/align="justify"/', 'style="text-align:justify;"', $about_company_block_text ); ?>
                            <div class="p"><?= $about_company_block_text ?> </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 col-xxl-7 offset-xxl-1">
                        <div class="about-company-video">
							<?php if ( get_field( 'about_company_block_video' ) ) : ?>
								<?php the_field( 'oembed' ); ?>
							<?php else: ?>
                                <img src="<?= get_field( 'about_company_image' ) ?>" class="about-img" alt="">
                                
							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="offer" class="offer">
            <div class="bg-img">
                <img src="<?= get_field( 'special_offer_block_image' ) ?>" alt="">
            </div>
            <div class="container">
                <div class="custom-border-top center-block text-center h3"><?= get_field( 'special_offer_title' ) ?></div>
                <div class="offer-content-block">
                    <span class="top-content d-block text-center text-lg-start c-white"><?= get_field( 'special_offer_block_text_1' ) ?></span>
                    <span class="bottom-content d-block text-center text-lg-start c-white"><?= get_field( 'privacy_policy_text', 'options' ) ?></span>
                    <button type="button" class="custom-btn c-white" data-bs-toggle="modal"
                            data-bs-target="<?= get_field( 'special_offer_block_button_url' ) ?>"><?= get_field( 'special_offer_block_button_text' ) ?></button>
                </div>
            </div>
        </section>
        <section class="reviews">
            <div class="container p-0">
                <h3 class="custom-border-top text-center div-h3 h3"><?= get_field( 'reviews_block_title' ) ?></h3>
                <p class="heading-text"><?= get_field( 'reviews_title_caption' ) ?></p>
                <div class="revievs-content-carousel">
                    <!-- Slider main container -->
                    <div class="swiper-container">
                        <!-- Additional required wrapper -->
                        <div class="swiper-wrapper">
                            <!-- Slides -->
							<?php if ( have_rows( 'reviews' ) ): $i = 1; ?>
								<?php while ( have_rows( 'reviews' ) ): the_row(); ?>
                                    <div class="swiper-slide">
                                        <div class="item-inner">
                                            <div class="item-top-content d-flex">
												<?php if(get_sub_field( 'photo' )) : ?>
                                                <img src="<?php the_sub_field( 'photo' ); ?>" alt="">
												<?php endif; ?>
                                                <span>
                                                <span class="div-h4 h4"><?php the_sub_field( 'name' ); ?></span>
                                                <span style="display: block" class="p"><?php the_sub_field( 'name_caption' ); ?></span>

                                            </span>
                                            </div>
                                            <div class="item-bottom-content">
                                                <p class="text-start"><?= the_sub_field( 'review_text' ) ?></p>
                                            </div>
                                        </div>
                                    </div>
								<?php endwhile; ?>
							<?php endif; ?> <!-- if( have_rows('reviews') ) -->
                        </div>
                        <div class="d-flex w-100 justify-content-center align-items-center">
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-pagination"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="form" style="background-image: url(<?= get_field('feedback_block_image') ?>);">
            <div class="container">
                <div class="text-center c-grey div-h3 h3"><?= get_field( 'feedback_block_title' ) ?></div>
				<?= do_shortcode( '[contact-form-7 id="207" title="Контактная Форма"]' ) ?>
            </div>
        </section>
    </main>

<?php
get_footer();