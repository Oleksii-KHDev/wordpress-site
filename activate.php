<?php
/**
 * Template Name: Activate
 * Template Post Type: page
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <title><?php the_title() ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="<?= WP_CONTENT_URL . '/uploads' . '/favicon.svg' ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<?php do_shortcode('[gold-bull-email-verification]')?>
<?php wp_footer(); ?>
</body>

</html>
