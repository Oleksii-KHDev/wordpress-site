<?php
if ( isset($_GET['id']) && isset($_GET['passkey']) ) {
    $upk  = htmlspecialchars( $_GET['passkey'] );
    $uid  = htmlspecialchars( $_GET['id'] );
    $hash = get_user_meta( $uid, 'hash', true );
    if ( !empty( $hash ) && $hash === $upk ) {
        $user = get_userdata($uid);
        $user->set_role( 'Free' );

        $date = date( 'Y-m-d H:i:s', time() );
        $pms_member_subscription = new PMS_Member_Subscription();
        $pms_member_subscription->insert([
            'user_id' => $user->ID,
            'subscription_plan_id' => 213,
            'start_date' => $date,
            'expiration_date' => '0000-00-00 00:00:00',
            'status' => 'active'
        ]);

//
//
        wp_set_current_user( $user->ID, $user->user_login );
        wp_set_auth_cookie( $user_id );
        do_action( 'wp_login', $user->user_login );

        $creds = array();
        $creds['user_login'] = $user->user_login;
        $creds['user_password'] = get_user_meta( $uid, 'pass', true );
        $creds['remember'] = true;

        $user = wp_signon( $creds, false );

        if ( !is_wp_error($user) ) {
            delete_user_meta( $user->ID, 'pass', $creds['user_password'] );
        }
    }

}
get_header();
if ( isset($_GET['id']) && isset($_GET['passkey']) ) {
    $upk  = htmlspecialchars( $_GET['passkey'] );
    $uid  = htmlspecialchars( $_GET['id'] );
    $hash = get_user_meta( $uid, 'hash', true );

    if ( !empty( $hash ) && $hash === $upk ) {
        $user = get_userdata( $uid );
//        $user->set_role( 'Free' );

        delete_user_meta( $user->ID, 'hash', $hash );
//        $date = date( 'Y-m-d H:i:s', time() );
//        $pms_member_subscription = new PMS_Member_Subscription();
//        $pms_member_subscription->insert([
//            'user_id' => $user->ID,
//            'subscription_plan_id' => 213,
//            'start_date' => $date,
//            'expiration_date' => '0000-00-00 00:00:00',
//            'status' => 'active'
//        ]);
        ?>
        <svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
            <symbol id="check-circle-fill" fill="currentColor" viewBox="0 0 16 16">
                <path d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-3.97-3.03a.75.75 0 0 0-1.08.022L7.477 9.417 5.384 7.323a.75.75 0 0 0-1.06 1.06L6.97 11.03a.75.75 0 0 0 1.079-.02l3.992-4.99a.75.75 0 0 0-.01-1.05z"/>
            </symbol>
        </svg>
        <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 100000">
            <div id="ToastMessage" class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true">
                <div class="toast-header justify-content-around">
                    <img class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Success:" src="<?= WP_CONTENT_URL . '/uploads' . '/favicon.svg' ?>" alt="">
                    Gold-Bull
                    <?php
                    $user_name = get_user_meta( $user->ID, 'first_name', true );
                    ?>
                    <div class="d-flex justify-content-around">
                        <strong class="me-auto"><?= $user_name ?>, Спасибо за регистрацию!</strong>
                        <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
                    </div>
                </div>
                <div class="toast-body">
                    <?php if($_GET['link_type'] == 'report') : ?>Вот ваш отчет!<?php endif; ?>Сейчас вам доступен скрытый контент.
                </div>
            </div>
        </div>
        <?php if($_GET['link_type'] == 'report') : ?>
            <?php
            echo "        
            <script>
                setTimeout(()=> {
                    let a = document.createElement('a');
                    a.setAttribute('target', '_blank');
                    a.id = 'link_to_report';
                    a.href = '" . get_field('link_on_report_file', 'options')  . "';
                    document.body.appendChild(a);
                    a.click();
                }, 5000 ); 
            </script>"; ?>
        <?php endif; ?>
        <?php
    }
}
?>
<main class="articles-page">
		<div class="container">
			<h1 class="text-center h1"><?= esc_html( get_the_title(get_queried_object_id()) ); ?></h1>
			<div class="row w-100 m-0">
				<?php if ( have_posts() ) : global $wp_query; $col = 7;
				    while ( have_posts() ) : the_post(); ?>
                        <div class="col-12 col-md-<?= $col ?> articles-page-content" data-link="<?= get_the_permalink() ?>" onclick="window.location = '<?= get_the_permalink() ?>'">
                        <?php if ( $wp_query->current_post % 2 == 0 ) :
                            $col = $col === 7 ? 5 : 7;
                        endif;?> <!-- if ( $wp_query->current_post % 2 == 0 ) -->
                            <a href="<?= get_the_permalink() ?>" target="_blank">
                                <img src="<?= get_the_post_thumbnail_url(); ?>" alt="">
                            </a>
                            <div class="articles-content-box">
                                <div class="div-h4 h4"><?= the_title() ?></div>
                                <div class="bottom-content d-flex d-lg-none justify-content-end align-items-stretch" style="height: 50px; margin-right: 10px;">
                                    <span class="d-block d-lg-none"><?= get_the_date() ?></span>
                                </div>
                                <a href="<?= get_the_permalink() ?>" class="custom-btn d-lg-none">Подробнее</a>

                                <div class="bottom-content d-none d-lg-flex justify-content-between center-block">
                                    <a href="<?= get_the_permalink() ?>">Подробнее</a>
                                    <span class="d-none d-lg-block"><?= get_the_date() ?></span>
                                </div>
                            </div>
                        </div>
                    <? endwhile; ?>
                <?php endif; ?>
			</div>
		</div>
	</main>
<?php
get_footer();
