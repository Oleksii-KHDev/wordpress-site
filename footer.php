<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Twenty_One
 * @since Twenty Twenty-One 1.0
 */
?>
<footer class="footer c-white overflow-hidden">
    <div class="container">
        <div class="row">
            <div class="col-12 d-lg-none footer-logo-heading">
                <div class="text-center h2 div-h2"><?= __('GOLD BULL', 'gold-bull') ?></div>
            </div>
            <div class="d-md-flex justify-content-md-between">
                <div class="d-none d-lg-block col-lg-3 text-lg-start">
                    <?php if( !is_front_page() ): ?>
                        <a href="<?= home_url() ?>">
                    <?php endif; ?>
                            <svg width="109" height="39" viewBox="0 0 109 39" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M35.4911 0H2.9306C2.15336 0 1.40794 0.313376 0.858349 0.871185C0.308755 1.42899 0 2.18555 0 2.97441V36.0215C0 36.8104 0.308755 37.5669 0.858349 38.1247C1.40794 38.6826 2.15336 38.9959 2.9306 38.9959H35.4911C36.2687 38.9959 37.0145 38.6827 37.5647 38.125C38.115 37.5673 38.4246 36.8108 38.4257 36.0215V2.97441C38.4246 2.18518 38.115 1.42863 37.5647 0.870937C37.0145 0.313244 36.2687 -7.32459e-07 35.4911 0ZM4.13403 15.2859C4.62179 14.6935 5.44141 14.1862 5.8692 13.6952C6.46092 13.0135 5.53736 12.0478 3.78619 13.1799C3.94541 13.3095 4.06096 13.4861 4.11683 13.6853C4.17271 13.8846 4.16614 14.0965 4.09804 14.2917C3.81018 15.1398 2.69471 15.4077 2.43084 16.2557C2.29307 15.7523 2.27833 15.2223 2.3879 14.7117C2.49747 14.2012 2.72804 13.7255 3.05964 13.3257C3.39123 12.926 3.81386 12.6144 4.29088 12.4179C4.76791 12.2214 5.28496 12.1459 5.79723 12.1979C6.87272 12.3643 7.23254 13.4964 5.93316 14.5839C5.59062 14.8569 5.26608 15.1524 4.96163 15.4685C4.42189 16.0691 4.86168 16.5804 5.59733 16.3856C7.41646 15.9027 9.67538 13.6506 10.7229 12.0194C12.502 9.24784 13.2177 4.88971 17.2838 5.60389C18.5991 5.83519 19.9345 6.41545 21.1939 6.88616C21.5937 7.04036 22.0295 7.17427 22.4453 7.31629C24.6282 7.94932 26.2875 8.25772 27.9667 6.43981C27.7508 8.21309 26.2755 9.1464 24.8641 9.38581L25.2919 13.8494C24.5229 14.2057 23.6654 14.3161 22.8331 14.1659C22.4813 13.1353 21.9695 12.5956 21.2658 11.8327C20.8432 12.0182 20.4076 12.1715 19.9625 12.2912C20.3785 12.2847 20.792 12.2261 21.1939 12.1167L21.5937 12.5225L21.7416 15.5618C21.9695 17.3189 20.942 18.0655 19.6106 18.8771L19.5027 18.938C19.2748 18.3765 19.2748 17.7464 19.5027 17.185L19.5547 17.1282C20.4942 16.6575 20.3343 15.7526 20.1584 14.8517C19.0549 15.3265 16.3082 16.552 15.0568 16.0447C15.2355 16.2918 15.4452 16.5141 15.6805 16.7062C14.3726 17.3322 13.142 18.1127 12.0143 19.0313C11.9783 19.0597 11.8503 19.1774 11.7544 19.2464C11.4705 21.5147 8.55591 22.6347 6.72079 23.3042C6.83204 24.9897 6.6063 26.6805 6.05711 28.2751C5.39484 27.7865 4.9392 27.0617 4.78171 26.2462C5.30118 24.9511 5.42515 23.5274 5.13754 22.1599C5.68878 21.7439 6.1206 21.1861 6.38839 20.5441C6.65618 19.9021 6.75023 19.1991 6.66082 18.5079C6.6198 18.0777 6.63324 17.6441 6.7008 17.2175C4.3939 18.3009 3.06654 16.5844 4.13403 15.2859ZM14.1292 32.7347H12.486V29.7481H14.1292V32.7347ZM17.4197 32.7347H15.7965V27.8166H17.4397L17.4197 32.7347ZM20.7061 32.7347H19.0629V25.0248H20.7061V32.7347ZM23.9966 32.7347H22.3533V22.9471H23.9966V32.7347ZM27.287 32.7347H25.6438V19.498H27.287V32.7347ZM30.5734 32.7347H28.9302V16.418H30.5894L30.5734 32.7347ZM33.8638 32.3573H32.2206V13.4071H33.8638V32.3573ZM32.8443 12.7254L32.0807 11.9707L20.4862 23.3326L20.2463 23.568L19.7146 24.0509L16.8919 21.1779L5.61332 32.3735L4.54583 32.264L16.4721 20.342L16.9239 19.9119L17.1478 20.1391L19.7946 22.8416L31.501 11.3985L30.6454 10.5504L33.8798 9.5887L32.8443 12.7254Z" fill="white"/>
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M6.39697 21.9365C7.54842 21.5307 9.19564 20.2525 9.67141 18.9418C9.67626 19.519 9.55902 20.0905 9.32758 20.6177C9.93129 19.9847 11.0628 18.2601 10.0392 16.2434C10.4316 16.4834 10.7739 16.7991 11.0468 17.1726C11.906 16.3261 12.8841 15.6136 13.9494 15.0585C13.6128 14.4774 13.4412 13.8131 13.4536 13.1391C15.3967 15.6956 19.0509 14.2388 20.8261 12.9768C19.8805 13.1899 18.893 13.1006 17.9994 12.7211C16.6321 12.0922 15.3847 10.3716 15.9324 8.6146C15.9809 9.01367 16.1451 9.38912 16.4042 9.69328C16.6634 9.99745 17.0057 10.2166 17.3877 10.323C17.1277 9.96441 16.9587 9.54627 16.896 9.10561C17.919 9.6287 19.0901 9.77224 20.2064 9.51139C20.1791 9.28099 20.1976 9.04736 20.2608 8.82433C20.324 8.6013 20.4305 8.3934 20.5742 8.21288C20.5262 9.56008 22.0215 10.5137 22.7132 11.7757C22.7256 11.3362 22.6675 10.8977 22.5413 10.4772C23.2555 11.3064 23.7325 12.3181 23.9206 13.4029C24.0928 13.3797 24.2636 13.3472 24.4324 13.3055L24.0606 9.39776C23.7244 9.32103 23.408 9.17286 23.1324 8.96303C22.8567 8.7532 22.628 8.48647 22.4613 8.18041L22.2174 8.11143H22.1934C21.7696 7.96535 21.3378 7.82737 20.914 7.66912C20.5942 7.55144 20.2664 7.42565 19.9345 7.2958L19.7186 7.21464C18.8859 6.87277 18.024 6.60926 17.1438 6.42741C16.7565 6.33735 16.3554 6.32687 15.964 6.39655C15.5726 6.46624 15.1989 6.6147 14.8649 6.8332C12.7579 8.29402 12.646 11.4064 11.4346 13.2974C11.4695 12.9862 11.4789 12.6727 11.4626 12.36C10.6471 13.6264 9.63752 14.7525 8.47198 15.6956C8.08916 16.0543 7.78769 16.4932 7.58841 16.9819C7.34452 17.6352 7.46847 18.3656 7.48846 19.0433C7.53244 20.3377 7.23657 21.1331 6.39697 21.9365Z" fill="white"/>
                                <path d="M57.9483 16.049C57.1606 16.7372 56.2479 17.2624 55.2616 17.595C54.2445 17.9388 53.1791 18.1129 52.1071 18.1104C51.0443 18.1119 49.9883 17.9378 48.9806 17.595C48.0035 17.2687 47.096 16.7579 46.3059 16.0896C45.4848 15.3993 44.8277 14.5301 44.3833 13.5466C43.9389 12.563 43.7187 11.4904 43.7391 10.4086C43.7203 9.28416 43.9656 8.17136 44.4547 7.1623C44.9143 6.23952 45.5584 5.42415 46.3458 4.76818C47.145 4.11216 48.0575 3.61268 49.0366 3.29518C50.0361 2.95989 51.0823 2.79 52.1351 2.79201C53.1661 2.79088 54.1915 2.94541 55.1776 3.25055C56.1347 3.54347 57.0352 4.00048 57.8404 4.6018L55.9492 6.82958C55.3984 6.45054 54.7908 6.16415 54.1501 5.98147C53.5004 5.79004 52.8276 5.69168 52.1511 5.6893C51.4742 5.68712 50.8007 5.78557 50.152 5.98147C49.5297 6.16744 48.9448 6.46421 48.4248 6.85796C47.9092 7.25075 47.4916 7.76084 47.2054 8.34721C46.8886 9.01039 46.7323 9.74076 46.7496 10.4776C46.726 11.1714 46.8686 11.8606 47.1653 12.4861C47.462 13.1117 47.9038 13.6548 48.4528 14.0688C48.9762 14.4642 49.5653 14.7611 50.192 14.9453C50.8409 15.1403 51.5143 15.2387 52.191 15.2374C53.2336 15.2408 54.2608 14.983 55.1816 14.4867V12.1738H53.6983L52.4989 9.47528H57.9563L57.9483 16.049Z" fill="white"/>
                                <path d="M78.0271 10.3148C78.0468 11.4506 77.803 12.5752 77.3155 13.5976C76.8578 14.531 76.217 15.3595 75.4324 16.0324C74.6381 16.7093 73.7253 17.2283 72.7417 17.5621C71.729 17.9148 70.6658 18.0944 69.5952 18.0937C68.5297 18.0953 67.4711 17.9212 66.4607 17.5784C65.4828 17.2512 64.5741 16.7405 63.7819 16.0729C62.9978 15.4037 62.3616 14.574 61.9148 13.6382C61.4334 12.6183 61.1938 11.4983 61.2152 10.3676C61.1951 9.2435 61.4391 8.13071 61.9268 7.12131C62.3866 6.19639 63.0338 5.38051 63.8259 4.72716C64.6251 4.07115 65.5375 3.57166 66.5166 3.25416C67.519 2.91891 68.5678 2.74903 69.6232 2.75099C70.6802 2.7506 71.7313 2.91064 72.7417 3.22578C73.7194 3.5213 74.6326 4.00332 75.4324 4.64603C76.2671 5.31953 76.936 6.1808 77.3863 7.16183C77.8367 8.14285 78.056 9.21674 78.0271 10.2986V10.3148ZM75.0406 10.3392C75.0618 9.65597 74.918 8.97786 74.6219 8.36395C74.3257 7.75004 73.8861 7.21895 73.3414 6.81697C72.818 6.43888 72.2294 6.16361 71.6062 6.0054C70.9545 5.82711 70.2821 5.73839 69.6072 5.74163C68.9315 5.73937 68.2587 5.83225 67.6081 6.01758C66.9838 6.19253 66.3978 6.48578 65.8809 6.88188C65.084 7.56179 64.5333 8.49212 64.316 9.52553C64.0988 10.5589 64.2274 11.6364 64.6815 12.5872C64.9771 13.1675 65.398 13.6724 65.9129 14.0643C66.4336 14.459 67.022 14.7521 67.6481 14.9286C68.2991 15.1117 68.9716 15.2045 69.6471 15.2045C70.3169 15.2061 70.9825 15.0978 71.6182 14.884C72.2432 14.6806 72.827 14.3644 73.3414 13.9507C73.8548 13.5424 74.2749 13.026 74.5728 12.4371C74.8971 11.7877 75.0578 11.067 75.0406 10.3392Z" fill="white"/>
                                <path d="M81.7051 17.6719V3.24219H84.6397V14.7665H92.1841V17.6719H81.7051Z" fill="white"/>
                                <path d="M93.0078 3.24217H101.804C102.796 3.23203 103.783 3.38692 104.726 3.70071C105.572 3.98567 106.347 4.4505 107.001 5.06414C107.645 5.68853 108.146 6.44908 108.468 7.29189C108.832 8.26477 109.008 9.29918 108.988 10.3393C109.002 11.3736 108.831 12.4019 108.484 13.3746C108.17 14.2436 107.674 15.0329 107.029 15.6876C106.377 16.3295 105.601 16.8269 104.75 17.1484C103.808 17.503 102.809 17.6777 101.804 17.6638H96.0224V6.11107H94.2672L93.0078 3.24217ZM99.005 6.11107V14.7665H101.804C103.171 14.7665 104.226 14.3891 104.974 13.6262C105.722 12.8633 106.094 11.788 106.094 10.3799C106.094 8.97186 105.722 7.90464 104.974 7.17829C104.226 6.45193 103.171 6.09482 101.804 6.09482L99.005 6.11107Z" fill="white"/>
                                <path d="M59.5637 31.2697C59.5637 32.7197 59.1199 33.8342 58.2323 34.6133C57.3447 35.3924 56.1066 35.782 54.5181 35.782H45.7822V21.3604H54.5061C55.0718 21.3577 55.6335 21.4566 56.1653 21.6526C56.6708 21.8398 57.1363 22.1235 57.5366 22.4885C57.9276 22.8476 58.2443 23.2821 58.4682 23.7667C58.7022 24.2873 58.8197 24.8541 58.812 25.4264C58.8157 25.8892 58.7455 26.3497 58.6041 26.7898C58.4653 27.2205 58.2138 27.6048 57.8765 27.9017C58.4177 28.2279 58.8485 28.7132 59.1119 29.2935C59.409 29.9086 59.5635 30.5846 59.5637 31.2697ZM48.6169 27.5121C49.2529 27.1389 49.9579 26.9027 50.6879 26.8182C51.476 26.7225 52.269 26.6751 53.0627 26.6762H54.5181C55.4669 26.6762 55.9414 26.2596 55.9414 25.4264C55.9509 25.2558 55.9195 25.0855 55.8499 24.93C55.7802 24.7745 55.6744 24.6385 55.5416 24.5336C55.2413 24.3203 54.8802 24.2133 54.5141 24.2293H48.6169V27.5121ZM48.6169 32.8847H54.5181C54.7981 32.8869 55.0777 32.8638 55.3537 32.8157C55.6002 32.7765 55.8374 32.6911 56.0533 32.5641C56.2566 32.4431 56.4259 32.2713 56.5451 32.065C56.6839 31.8053 56.7503 31.5121 56.737 31.2169C56.7535 30.9865 56.7149 30.7554 56.6244 30.5435C56.5339 30.3315 56.3942 30.1449 56.2172 29.9996C55.7066 29.6873 55.1123 29.5454 54.5181 29.5938H53.0627C52.4031 29.5938 51.8633 29.5938 51.4315 29.6343C51.059 29.6554 50.688 29.6974 50.3201 29.7601C50.0275 29.8099 49.7417 29.8943 49.4685 30.0117C49.2126 30.1213 48.9287 30.2592 48.6169 30.4175V32.8847Z" fill="white"/>
                                <path d="M70.0985 36.2283C69.1759 36.2372 68.2566 36.1143 67.3678 35.8631C66.5812 35.647 65.8446 35.2746 65.2008 34.7675C64.5807 34.2626 64.0908 33.6123 63.7735 32.8725C63.4156 32.0047 63.2425 31.0698 63.2657 30.1294V21.3604H66.0924V30.1294C66.0755 30.656 66.1738 31.1798 66.3802 31.6632C66.5648 32.0595 66.8489 32.3996 67.2038 32.6493C67.5886 32.9125 68.0228 33.0922 68.4792 33.1768C69.5667 33.3824 70.6822 33.3824 71.7697 33.1768C72.2261 33.0922 72.6603 32.9125 73.0451 32.6493C73.4 32.3996 73.6841 32.0595 73.8687 31.6632C74.0751 31.1798 74.1734 30.656 74.1565 30.1294V21.3604H76.9672V30.1294C76.9884 31.0697 76.8154 32.0041 76.4594 32.8725C76.1421 33.6123 75.6523 34.2626 75.0321 34.7675C74.3883 35.2746 73.6517 35.647 72.8651 35.8631C71.9763 36.1143 71.0571 36.2372 70.1345 36.2283H70.0985Z" fill="white"/>
                                <path d="M82.3047 35.7819V21.3604H85.2393V32.8846H92.7797V35.7819H82.3047Z" fill="white"/>
                                <path d="M95.9102 35.7819V21.3604H98.8448V32.8846H106.389V35.7819H95.9102Z" fill="white"/>
                            </svg>
	                <?php if( !is_front_page() ): ?>
                        </a>>
                    <?php endif; ?>
<!--                    <a href="https://t.me/gold_bull_invest" target="_blank">-->
                    <div class="button">
                        <div class="holder">
                            <svg width="28" height="29" viewBox="0 0 28 29" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path id="Vector" d="M9.97106 16.8145L15.0607 13.5994L11.3328 16.9786L11.1837 17.1137L11.1698 17.3144L10.9214 20.8884L10.8842 21.423H11.4202C11.6822 21.423 11.9002 21.3634 12.0906 21.2522C12.2559 21.1558 12.3823 21.0287 12.4674 20.9431C12.4706 20.9399 12.4737 20.9368 12.4767 20.9338L13.8568 19.6L17.0164 21.9368L17.0406 21.9546L17.0667 21.9695C17.4443 22.1841 17.8997 22.3121 18.3284 22.1175C18.7548 21.9239 18.9681 21.4952 19.0666 21.0474L19.0674 21.0435L21.3648 10.1808C21.3648 10.1808 21.3648 10.1807 21.3648 10.1807C21.485 9.61778 21.4126 9.07814 21.051 8.72472C20.6828 8.36497 20.1674 8.34528 19.7348 8.50167L19.7348 8.50155L19.7244 8.50556L6.22684 13.7278L6.22496 13.7286C5.96049 13.8321 5.73084 13.9586 5.55579 14.1129C5.38331 14.2649 5.2252 14.4801 5.20136 14.7557C5.17615 15.047 5.3104 15.2863 5.48388 15.45C5.65024 15.6069 5.86863 15.7139 6.09817 15.7866L6.09978 15.7871L9.55462 16.8689L9.77543 16.9381L9.97106 16.8145ZM0.5 14.5C0.5 7.0148 6.54385 0.953125 14 0.953125C21.4562 0.953125 27.5 7.0148 27.5 14.5C27.5 21.9852 21.4562 28.0469 14 28.0469C6.54385 28.0469 0.5 21.9852 0.5 14.5Z" fill="#3F81C6"  fill-rule="evenodd" stroke="white"/>
                            </svg>
                        </div>
                        <span class="title">Наш телеграмм канал</span>
                    </div>
<!--                    </a>-->
                </div>
                <div class="d-none d-md-block col-md-4 col-lg-2 main-footer">
                    <?php if( is_front_page() ): ?>
                        <div class="div-h2 h2"><?= get_field('footer_columns_text_for_first_column_header', 'options') ?></div>
                    <?php else: ?>
                    <a href="<?= get_field('footer_columns_page_for_1_column','options') ?>">
                        <div class="div-h2 h2"><?= get_field('footer_columns_text_for_first_column_header', 'options') ?></div>
                    </a>
                    <?php endif; ?>
                        <?php
                        wp_nav_menu( [
                            'theme_location'  => 'footer',
                            'container' => false,
                            'without_li'        => true,
                            'menu_id' => false,
                            'menu_class'      => false,
                            'items_wrap' => '%3$s',
                            'item_spacing' => 'preserve',
                            'fallback_cb'     => '__return_empty_string',
                            'depth'           => 0,
                        ] );
                        ?>

                </div>
                <div class="d-none d-md-block col-md-4 main-footer text-center">
                    <span class="d-block text-start middle-span">
                        
                        <?php if(substr_count($_SERVER['REQUEST_URI'], 'articles') > 0): ?>
                            <span class="div-h2 h2"><?= get_field('footer_columns_text_for_second_column_header', 'options') ?></span>
                        <?php else: ?>
                        <a href="<?= get_field('footer_columns_page_for_column_2','options') ?>">
								<span class="div-h2 h2"><?= get_field('footer_columns_text_for_second_column_header', 'options') ?></span>
                        </a>
                        <?php endif; ?>
                        <?php $posts = get_posts( array(
                            'numberposts' => 3,
                            'orderby'     => 'date',
                            'order'       => 'DESC',
                            'suppress_filters' => true,
                        ) );
                        global $post;
                        foreach ($posts as $article) : ?>
                             <?php if($post->ID == $article->ID): ?>
                                <span><?= $article->post_title ?></span>
                             <?else: ?>
                                <a href="<?= get_permalink($article->ID) ?>"><?= $article->post_title ?></a>
                             <?endif; ?>
                        <?php endforeach;?>
                    </span>
                </div>
                <div id="contacts" class="col-12 col-md-4 col-lg-3 contacts">
                    <div class="div-h2 h2"><?= get_field('footer_columns_text_for_contacts_column','options') ?></div>
                    <div class="span">
                            <a href="tel:<?= get_field('footer_columns_phone_url','options') ?>" class="d-block"><?= get_field('footer_columns_phone_text','options') ?></a>
							<a href="mailto:<?= get_field('footer_columns_email_url','options') ?>" class="d-block"><?= get_field('footer_columns_email_text','options') ?></a>
                            <?php if(is_front_page() || is_home()): ?>
                                <span class="d-none d-md-block">Поделиться:</span>
			                    <?= do_shortcode('[social-share]') ?>
                            <?php endif; ?>

                    </div>

                </div>
            </div>
        </div>
        <div class="row footer-bottom">
            <div class="col-12 col-md-4 d-md-flex justify-content-start align-items-start p-0">
                <a href="<?= get_field('privacy_policy_link', 'options') ?>" class="mb-3 mb-md-0 d-block"><?= get_field('privacy_policy_link_text', 'options') ?></a>
                <?php $page_in_footer = get_field('page_in_footer_bottom', 'options')?>
                <a href="<?= get_permalink( $page_in_footer )?>" class="mb-3 mb-md-0 d-block link_in_footer"><?= $page_in_footer->post_title ?></a>
            </div>
            <div class="col-12 col-md-4">
                <p class="mb-3 mb-md-0"><?= date('Y') . ' ' . get_field('copyright_text', 'options') ?></p>
            </div>
            <div class="col-12 col-md-4">
<!--                <a target="_blank" href="https://hardevs.io/"><img src="--><?//= WP_CONTENT_URL . '/uploads/HarDevs_logo.png' ?><!--" alt="" class="bottom-logo"></a>-->
                <a target="_blank" href="<?= get_permalink(817) ?>" style="font-size: 15px;">Карта сайта</a>
            </div>

        </div>
    </div>
</footer>

<?php wp_footer(); ?>
<?php
echo "
<script>
    let hustle_button = document.getElementsByClassName('hustle-button-cta');
//        console.log(hustle_button);
        if(hustle_button[0] != null && hustle_button[0] != undefined ) {
            hustle_button[0].addEventListener('click', function (e) {
                e.preventDefault();
                let register_button = document.querySelectorAll('.signin.c-white.report')[0];
//                console.log(register_button);
                document.getElementById('report_message').style.display = 'block';
                //let rep_buttons = document.getElementsByClassName('report');
                register_button.click();
            });
        }
</script>
";
?>

</body>

</html>