<?php

class GoldBull {

	/**
	 * GoldBull constructor.
	 */
	public function __construct() {
		/**
		 * Defines params
		 */



		define( "THEME_VERSION", "0.0.1" );
		define( "MAILCHIMP_API_KEY", "e9b5cfa88df017399626c98174b4a31b-us5" );
		define( "MAILCHIMP_LIST_ID", "e8ab3d57ea" );

		/**
		 * Register action hooks
		 */


        add_action( 'wp_footer', [$this, 'gold_bull_add_swipe_to_slider'], 999 );
        add_action( 'wp_head', [$this, 'gold_bull_add_style_for_super_cycle_block']);
        add_action( 'wp_head', [$this, 'gold_bull_add_Google_Tag_Manager_in_HEAD_tag']);
        add_action( 'wp_body_open', [$this, 'gold_bull_add_Google_Tag_Manager_in_BODY_tag'] );
		add_action( 'after_setup_theme', [ $this, 'gold_bull_theme_setup'] );
		add_action( 'after_setup_theme', [ $this, 'gold_bull_register_nav_menu' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'gold_bull_wp_enqueue_scripts_and_styles' ] );
		add_action( 'register_new_user', [ $this, 'gold_bull_register_new_user' ] );
		//add_action( 'phpmailer_init', [ $this, 'gold_bull_phpmailer_init' ] );
		add_action( 'user_register', [ $this, 'gold_bull_user_register_hook' ], 20, 1 );
		add_action( 'delete_user', [ $this, 'gold_bull_user_delete_hook' ], 20, 1 );
        add_action( 'wp_authenticate', [$this, 'gold_bull_chek_if_user_confirm_email'], 10, 2);
        add_action( 'init', [$this, 'gold_bull_add_user_and_new_role_on_site']);
        add_filter('autoptimize_filter_js_bodyreplacementpayload', [$this, 'gold_bull_autoptimize_filter_js_bodyreplacementpayload'], 1, 1);
//        add_action( 'plugins_loaded', [ $this, 'gold_bull_plugins_setup'] );
//        add_action('wp_head', function () { wp_enqueue_script( 'bootstrap', "https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js", array('jquery'), '', false ); }, 1 );
        add_filter( 'manage_users_columns', [$this, 'gold_bull_add_column_with_user_registration_date'] );
        add_filter( 'manage_users_custom_column', [$this, 'gold_bull_for_column_with_user_registration_date'], 25, 3 );
        add_filter( 'manage_users_sortable_columns', [$this, 'gold_bull_add_column_with_user_registration_date_to_sortable_column'], 100 );
        add_action( 'users_list_table_query_args', [$this,  'gold_bull_sort_users_in_admin']);
        add_filter( 'wp_sitemaps_add_provider', [$this, 'gold_bull_remove_sitemap_provider'], 10, 2 );
        add_filter( 'walker_nav_menu_start_el', [$this, 'gold_bull_remove_tag_a_on_articles_page'], 10, 4 );
        add_action( 'template_redirect', [$this, 'gold_bull_redirect_action']);
        add_filter( 'script_loader_tag', [$this, 'gold_bull_change_output_of_script_tag'], 10, 3  );


        /**
         * Will cancel the letter about the registration of a new user
         */
        remove_action( 'register_new_user', 'wp_send_new_user_notifications' );
        remove_action( 'edit_user_created_user', 'wp_send_new_user_notifications' );



        /**
		 * Register filters
		 */
		add_filter( 'wp_nav_menu_items', [$this, 'gold_bull_wp_nav_menu_items'], 1, 2);
		add_filter( 'the_content', [$this, 'gold_bull_edit_article_content'], 999 );

		/**
		 * Other theme settings
		 */
		if ( function_exists( 'acf_add_options_page' ) ) {
			acf_add_options_page( array(
				'page_title' => __( 'Основные настройки темы', 'gold-bull' ),
				'menu_title' => __( 'Настройки темы', 'gold-bull' ),
				'menu_slug'  => 'theme-general-settings',
				'capability' => 'edit_posts',
				'redirect'   => false
			) );
		}
		add_theme_support( 'post-thumbnails', array( 'post', 'page' ) ); // Theme support image for article
		remove_filter( 'the_excerpt', 'wpautop' );
	}

	function gold_bull_change_output_of_script_tag($tag, $handle, $src) {
	    $tag = preg_replace("/type='text\/javascript'/", '', $tag);
	    $tag = preg_replace("/type=\"text\/javascript\"/", '', $tag);

	    return $tag;
    }

	function gold_bull_redirect_action() {
        $number_rep = 0;
        $is_redirect = 0;
        $url = $_SERVER['REQUEST_URI'];
        $url = preg_replace("/\/\?[%3F]+/", "/", $url, -1, $number_rep);
        if ($number_rep > 0) $is_redirect = 1;
        $url = preg_replace("/\/{2,}/", "/", $url, -1, $number_rep);
        if ($number_rep > 0) $is_redirect = 1;
//    $url = preg_replace("/\/\?{2,}/", "/", $url, -1, $number_rep);
//    if ($number_rep > 0) $is_redirect = 1;

        if ($is_redirect > 0) { wp_redirect( site_url('/'), 301 ); }
    }
	function gold_bull_remove_tag_a_on_articles_page($item_output, $item, $depth, $args) {
//        global $post;
//        $id = $post->ID;
	    file_put_contents('555.txt', print_r($id . ' ',true), FILE_APPEND);
	    file_put_contents('777.txt', print_r($item_output, true), FILE_APPEND);
        file_put_contents('999.txt', print_r($item->object_id . ' ', true), FILE_APPEND);

	    if( $item->object_id == 35 && substr_count($_SERVER['REQUEST_URI'], 'articles') > 0 ) {
            $item_output = '<span class="articles-page">' . $item->title . '</span>';
        }
	    if( $item->object_id == 11 && is_front_page() ) {
            $item_output = '<span class="home-page">' . $item->title . '</span>';
        }
	    return $item_output;
    }

	function gold_bull_remove_sitemap_provider( $provider, $name ){

        // отключаем архивы пользователей
        if( in_array( $name, ['users','taxonomies'] ) )
            return false;

        return $provider;
    }

    function gold_bull_sort_users_in_admin($args) {
        // Сортировка по дате регистрации
        $args['orderby'] = empty( $_REQUEST['orderby'] ) ? 'registered' : $_REQUEST['orderby'];
        // DESC - список начинать с новых пользователей
        $args['order'] = empty( $_REQUEST['order'] ) ? 'DESC' : $_REQUEST['order'];
        return $args;
    }

    function gold_bull_add_column_with_user_registration_date_to_sortable_column ($columns) {
        $columns['registration_date'] = 'registered';
        return $columns;
    }

    function gold_bull_add_column_with_user_registration_date( $columns ) {

        $columns[ 'registration_date' ] = 'Дата регистрации';
        return $columns;

    }
    function gold_bull_for_column_with_user_registration_date( $row_output, $column_id, $user) {
        if( 'registration_date' == $column_id ) {
            // возвращаем, а не выводим!
            return get_the_author_meta( 'registered', $user );
        }

        return $row_output;
    }

    public function gold_bull_autoptimize_filter_js_bodyreplacementpayload($bodyreplacementpayload)
    {
        if(!is_admin()) {
            preg_match('/<script.*?src="(.*?)"/', $bodyreplacementpayload, $src);
            $src = $src[1];
            return '<script>function addScript(t,e){var d=document.createElement("script");e&&(d.onload=e),d.type="text/javascript",d.async="async",d.src=t,document.body.appendChild(d)}function loadAutoptimize(){addScript("' . $src . '",null)}if(!navigator.userAgent.match(/(Chrome-Lighthouse)/)) {addScript("' . $src . '",null)}else{setTimeout(loadAutoptimize,5000)};</script>';
        }
        return $bodyreplacementpayload;
    }

    public function gold_bull_add_Google_Tag_Manager_in_BODY_tag() {
    	?>
    	<!-- Google Tag Manager (noscript) -->
			<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NZB88GB"
				height="0" width="0" style="display:none;visibility:hidden"></iframe>
			</noscript>
		<!-- End Google Tag Manager (noscript) -->

    	<?php

    }

    public function gold_bull_add_Google_Tag_Manager_in_HEAD_tag() {
    	?>
    	<!-- Google Tag Manager -->
		<script>
            function gtag_script() {
                (function (w, d, s, l, i) {
                    w[l] = w[l] || [];
                    w[l].push({
                        'gtm.start':
                            new Date().getTime(), event: 'gtm.js'
                    });
                    var f = d.getElementsByTagName(s)[0],
                        j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : '';
                    j.async = true;
                    j.src =
                        'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
                    f.parentNode.insertBefore(j, f);
                })(window, document, 'script', 'dataLayer', 'GTM-NZB88GB');
            }
            setTimeout(gtag_script, 5000);
		</script>
		<!-- End Google Tag Manager -->
    	<?php
    }

    /**
     * Checks if a user has been created for the site manager.
     * Checks if a custom role has been created for the site manager.
     * If not - creates them.
     */

    public function gold_bull_add_user_and_new_role_on_site() {
        $is_role_created = (bool)get_role('SEO');

        if(!$is_role_created) {
            $admin = get_role( 'administrator' );
            add_role( 'SEO', 'SEO менеджер', $admin->capabilities );


            // remove some rights from the new role
            $manager = get_role( 'SEO' );

            $caps = array(
                'create_users',
                'delete_users',
                'edit_users',
                'list_users',
                'promote_users',
                'remove_users',
            );

            foreach ( $caps as $cap ) {
                $manager->remove_cap( $cap );
            }
        }


	        if((bool)get_role('manager')) {
		        remove_role( 'manager' );
	        }






        // checks if user for manager role have been created. If not - create him.

//        if( !email_exists(get_field('site_manager_email','options')) ){
//            $userdata = [
//                'user_login' => get_field('site_manager_email','options'),
//                'user_pass' => get_field('managers_password','options'),
//                'user_email' => get_field('site_manager_email','options'),
//                'display_name' => get_field('manager_name','options'),
//                'first_name' => get_field('manager_name','options'),
//
//                'role' => 'SEO',
//            ];
//            wp_insert_user( $userdata );
//        }

    }

	/**
	 * Confirmation of the user's mail and,
	 * if successful, changing the user's role
	 */

	public function gold_bull_add_style_for_super_cycle_block() {
        echo "
	    <style>           
            .super-cycle .super-cycle-content {
                background-image: url(". get_field('background_image') ." );
                background-size: cover;                
            }         
            @media only screen and (min-width: 768px) {
                .super-cycle .super-cycle-content {
                    background-image: none;
                }
                .super-cycle {              
                    background-image: url(". get_field('background_image') .");                    
                }
                
            } 
             
        </style>
        ";
    }

	public function gold_bull_edit_article_content($content) {
//        if( $GLOBALS['post']->post_type == 'post' ) {
//            $content .= $content . 'post';
//        }
        return $content;
    }

	public function gold_bull_chek_if_user_confirm_email($user_login, $user_password) {

        if ( $user = get_user_by( 'email', $user_login ) ) {
         if( get_user_meta( $user->ID, 'hash', true ) ) {
             echo $_REQUEST['callback'] . "(" . json_encode(['result' => false, 'error' => 'Для работы с сайтом вам необходимо подтвердить E-mail', 'action' => 'login']) . ")";
             exit;
         }
        }

    }


	public function gold_bull_add_swipe_to_slider() {
        if (is_front_page()) { ?>
        <script>
            window.clickOnDarkenedArticle = function (article) {

                let isDown = false,
                    start_x;
                let isMove = false;

                article.addEventListener('click', function(event) {
                    if(isMove) {
                        event.preventDefault();
                        isMove = false;
                    } else {
                        // переход по ссылке
                        window.location.href = article.getAttribute('data-article-link');
                    }
                });

                article.addEventListener('mousedown', function(event) {
                    event.preventDefault();
                    isDown = true
                    start_x = event.clientX
                }, true);

                article.addEventListener('mouseup', function() {
                    isDown = false;
                }, true);

                article.addEventListener('mousemove', function(event) {
                    event.preventDefault();

                    if (isDown) {

                        isMove = true;
                    }

                }, true);

            }

            window.bootstrapCarouselMove = function (carousel){

			let isDown = false,
				start_x;
			let isMove = false;

			carousel.addEventListener('click', function(event) {

				if(isMove) {
					event.preventDefault();
					isMove = false;
				}
			});

			carousel.addEventListener('mousedown', function(event) {
				event.preventDefault();
				isDown = true
				start_x = event.clientX
			}, true);

			carousel.addEventListener('mouseup', function() {
				isDown = false;
			}, true);

			carousel.addEventListener('mousemove', function(event) {
				event.preventDefault();

				if (isDown) {

					if (start_x < event.clientX){
						jQuery(carousel).carousel('prev');
					}else {
						jQuery(carousel).carousel('next')
					}

					isMove = true;
				}

			}, true);
			}

            let JS= document.createElement('script');
            JS.text = "bootstrapCarouselMove( document.getElementById('carouselArticlesIndicators') );";
            document.body.appendChild(JS);

            let articles = document.querySelectorAll('#carouselArticlesIndicators .carousel-item');
            articles.forEach(function (element) {
                clickOnDarkenedArticle(element);
            });
        </script>
        <?php
    	}
	}
	/**
	 * Settings for theme
	 * - load theme text domain for languages
	 */
	public function gold_bull_theme_setup(){
		load_theme_textdomain( 'gold-bull', get_template_directory() . '/languages' );
	}

    public function gold_bull_plugins_setup() {
        load_plugin_textdomain('contact-form-7');
    }

	/**
	 * @param $items
	 * @param $args
	 * @return array|mixed|string|string[]|null
	 */
	public function gold_bull_wp_nav_menu_items($items, $args = null)
	{
		if (isset($args->without_li) && $args->without_li) {
			return preg_replace('/<\/?li.*?>/', '', $items);
		}
		return $items;
	}

	public function gold_bull_phpmailer_init( &$phpmailer ) {
		if(strpos($phpmailer->Body, network_site_url('wp-login.php?action=rp')) !== false) {
			$phpmailer->ContentType = 'text/html';
			$phpmailer->Body = sprintf(__('Thank you for registering!<br><br>Here is your report: ', 'gold-bull') . '<a href="%1$s">%2$s</a>', get_field( 'link_on_report_file', 'options' ), __('Download report', 'gold-bull'));
		}
	}

	private function mailchimp_curl_connect( $url, $request_type, $api_key, $data = array() ) {
		if( $request_type == 'GET' )
			$url .= '?' . http_build_query($data);

		$mch = curl_init();
		$headers = array(
			'Content-Type: application/json',
			'Authorization: Basic '.base64_encode( 'user:'. $api_key )
		);
		curl_setopt($mch, CURLOPT_URL, $url );
		curl_setopt($mch, CURLOPT_HTTPHEADER, $headers);
		//curl_setopt($mch, CURLOPT_USERAGENT, 'PHP-MCAPI/2.0');
		curl_setopt($mch, CURLOPT_RETURNTRANSFER, true); // do not echo the result, write it into variable
		curl_setopt($mch, CURLOPT_CUSTOMREQUEST, $request_type); // according to MailChimp API: POST/GET/PATCH/PUT/DELETE
		curl_setopt($mch, CURLOPT_TIMEOUT, 10);
		curl_setopt($mch, CURLOPT_SSL_VERIFYPEER, false); // certificate verification for TLS/SSL connection

		if( $request_type != 'GET' ) {
			curl_setopt($mch, CURLOPT_POST, true);
			curl_setopt($mch, CURLOPT_POSTFIELDS, json_encode($data) ); // send data in json
		}

		return curl_exec($mch);
	}

	private function mailchimp_subscribe_unsubscribe( $email, $status, $merge_fields = array( 'FNAME' => '', 'LNAME' => '' ) ){
		/*
		 * please provide the values of the following variables
		 * do not know where to get them? read above
		 */
		$api_key = MAILCHIMP_API_KEY;
		$list_id = MAILCHIMP_LIST_ID;

		/* MailChimp API URL */
		$url = 'https://' . substr($api_key,strpos($api_key,'-')+1) . '.api.mailchimp.com/3.0/lists/' . $list_id . '/members/' . md5(strtolower($email));
		/* MailChimp POST data */
		$data = array(
			'apikey'        => $api_key,
			'email_address' => $email,
			'status'        => $status, // in this post we will use only 'subscribed' and 'unsubscribed'
			'merge_fields'  => $merge_fields // in this post we will use only FNAME and LNAME
		);
		return json_decode( $this->mailchimp_curl_connect( $url, 'PUT', $api_key, $data ) );
	}

	public function gold_bull_user_register_hook( $user_id ) {
		$user = get_user_by('id', $user_id ); // feel fre to use get_userdata() instead

		$subscription = $this->mailchimp_subscribe_unsubscribe( $user->user_email, 'subscribed', array( 'FNAME' => $user->first_name,'LNAME' => $user->last_name ) );

		/*
		 * if user subscription was failed you can try to store the errors the following way
		 */
		if( $subscription->status != 'subscribed' )
			update_user_meta( $user_id, '_subscription_error', 'User was not subscribed because:' . $subscription->detail );
	}

	public function gold_bull_user_delete_hook( $user_id ) {
		$user = get_user_by( 'id', $user_id );
		$subscription = $this->mailchimp_subscribe_unsubscribe( $user->user_email, 'unsubscribed', array( 'FNAME' => $user->first_name,'LNAME' => $user->last_name ) );
	}

	public function gold_bull_register_new_user( $user_id ) {

	    if(isset($_POST['user_password'])
		   && isset($_POST['user_repeat_password'])
		   && $_POST['user_password'] === $_POST['user_repeat_password']) {
			$user = get_user_by('id', $user_id);
			if(isset($_POST['user_name'])) {
				$user->first_name = $_POST['user_name'];
				wp_update_user($user);
			}
			wp_set_password( $_POST['user_password'], $user_id );
			update_user_meta( $user_id, 'default_password_nag', false );
            $hash = md5($user->user_email.$_POST['user_password']);
			add_user_meta( $user_id, 'hash', $hash );
            add_user_meta( $user_id, 'pass', $_POST['user_password'] );
		    $user->add_role( 'Free' );

            $headers = array(
                'From: ' . get_field('mail_from_field', 'options'),
                'content-type: text/html',
            );

//            $article_page = get_page_by_path('статьи');
//            $article_page = get_permalink(35);

            $link = get_permalink(35);
            $link .= '?id=' . $user_id .'&passkey=' . $hash . '&link_type=' . $_POST['action_type'];
		    $subject = get_field('confirm_mail_subject', 'options');
            $message = get_field('confirm_mail_text', 'options') . '<br>' . $link;


            wp_mail( $user->user_email, $subject, $message, $headers );
		} else {
			wp_delete_user($user_id);
		}
	}

	public function gold_bull_register_nav_menu() {
		register_nav_menus( array(
			'footer'     => 'Menu In Footer',
			'primary'    => 'Main Menu',
		) );
	}

	public function gold_bull_wp_enqueue_scripts_and_styles() {
		// Main scripts and styles
		$dev_or_prod = WP_DEBUG ? 'dist' : 'prod';
		$random = WP_DEBUG ? rand() : '';
        wp_enqueue_script( 'jquery' );
		wp_enqueue_style( 'main-styles', get_stylesheet_directory_uri() . "/$dev_or_prod/css/style.css?" . $random, [] );
		wp_enqueue_script( 'main-scripts', get_stylesheet_directory_uri() . "/$dev_or_prod/js/script.js", array('jquery'), THEME_VERSION . $random, true );


		// localize params for login with ajax
		wp_localize_script( 'login-with-ajax', 'data_for_forms', array(
			'password_error' => get_field( 'password_error_message', 'options' ),
			'link_on_pdf'    => get_field( 'link_on_report_file', 'options' ),
            'home_url'       => home_url('/')
		) );
	}
}

new GoldBull;

