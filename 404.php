<?php
get_header();
?>
<main class="container-fluid page-404">
	<div class="row">
    <div class="col-12 col-md-5 col-xl-6 block-404">
        404
        <div class="sorry-text">
            К сожалению, страница не найдена!
        </div>
    </div>
    <div class="d-none col-md-2 d-xl-none"></div>
    <div class="col-12 col-md-5 col-xl-6 article-block pt-5 pt-lg-0 pt-xl-4">
        <h2>Ознакомьтесь с нашими статьями</h2>
        <?= do_shortcode('[wp_sitemap_page only="post"]') ?>
    </div>
    </div>
</main>
<?php
get_footer();