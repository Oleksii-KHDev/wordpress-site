'use strict';

import 'bootstrap'
import "../css/style.css";
import "../scss/styles.scss";
import Swiper, { Navigation, Pagination, Autoplay } from 'swiper';
import { Modal } from 'bootstrap';
import { Toast } from 'bootstrap';




// window.bootstrapCarouselMove = function (carousel){
//
// 	let isDown = false,
// 		start_x;
// 	let isMove = false;
//
// 	carousel.addEventListener('click', function(event) {
//
// 		if(isMove) {
// 			event.preventDefault();
// 			isMove = false;
// 		}
// 	});
//
// 	carousel.addEventListener('mousedown', function(event) {
// 		event.preventDefault();
// 		isDown = true
// 		start_x = event.clientX
// 	}, true);
//
// 	carousel.addEventListener('mouseup', function() {
// 		isDown = false;
// 	}, true);
//
// 	carousel.addEventListener('mousemove', function(event) {
// 		event.preventDefault();
// 		if (isDown) {
//
// 			if (start_x < event.clientX){
// 				jQuery(carousel).carousel('prev');
//
//
// 			}else {
// 				jQuery(carousel).carousel('next')
//
// 			}
//
// 			isMove = true;
// 		}
// 	}, true);
// }


let header = document.getElementById('header')

window.onscroll = function () {
	if (document.body.scrollTop > 100 || document.documentElement.scrollTop > 100) {
		header.classList.add('header-scroll')
	}
	else {
		header.classList.remove('header-scroll')
	}
}

// let hustle_button = document.getElementsByClassName('hustle-button-cta');
// console.log(hustle_button);
// hustle_button[0].addEventListener('click', function (e) {
// 	e.preventDefault();
// 	let register_button = document.querySelectorAll('.signin.c-white.register')[0];
// 	console.log(register_button);
// 	register_button.click();
// });


let li = document.querySelectorAll('li.signin');
for (let i = 0; i < li.length; i++) {
	if(li[i]) {
		li[i].addEventListener('click', function (e) {
			// if(e.target.children[0]) {
			// 	e.target.children[0].click();
			// 	// console.dir(e);
			// }
			let button = e.target.querySelector('button');
			if(button) {
				button.click();
			}
		});
	}
}

document.addEventListener('DOMContentLoaded', function (){
	let rep_mess = document.getElementById('report_message');
	if(rep_mess != null && rep_mess != undefined) {
		rep_mess.style.display = 'block';
	}
});

let rep_buttons = document.getElementsByClassName('report');
if(rep_buttons != null && rep_buttons != undefined ) {
	for (let i = 0; i < rep_buttons.length; i++) {
		rep_buttons[i].addEventListener('click', function (e) {
			// console.log('here');
			let rep_mess = document.getElementById('report_message');
			if(rep_mess) {
				rep_mess.style.display = 'block';
				rep_mess.setAttribute('data-type', 'rep');
			}

			let form = document.getElementsByClassName('lwa-register-form');
			if (document.getElementsByName('action_type')[0]) {
				let el = document.getElementsByName('action_type')[0];
				if(el != null && el != undefined) {
					el.remove();
				}
			}
			let input = document.createElement('input');
			input.type = 'hidden';
			input.name = 'action_type';
			input.value = 'report';

			// console.dir(form);
			if(form[0] != null && form[0] != undefined) {
				form[0].prepend(input);
			}
		});
	}
}

let reg_buttons = document.getElementsByClassName('register');
if (reg_buttons != null && reg_buttons != undefined) {
	for (let i = 0; i < reg_buttons.length; i++) {
		if (reg_buttons[i] != null && reg_buttons != undefined) {
			reg_buttons[i].addEventListener('click', function (e) {
				// console.log('here');
				let rep_mess = document.getElementById('report_message');
				if(rep_mess) {
					rep_mess.style.display = 'none';
					rep_mess.setAttribute('data-type', 'reg');
				}

				let form = document.getElementsByClassName('lwa-register-form');
				if (form[0]) {
					if (document.getElementsByName('action_type')[0]) {
						let el = document.getElementsByName('action_type')[0];
						el.remove();
					}
					let input = document.createElement('input');
					input.type = 'hidden';
					input.name = 'action_type';
					input.value = 'register';

					// console.dir(form);
					form[0].prepend(input);
				}
			});
		}
	}
}

let reg_article_button = document.querySelectorAll('.login-buttons button[data-bs-target="#signinModal"]')
// console.log(reg_article_button[0]);
if(reg_article_button[0]) {

	reg_article_button[0].addEventListener('click', function (e) {
		let rep_mess = document.getElementById('report_message');
		if(rep_mess) {
			rep_mess.style.display = 'none';
			rep_mess.setAttribute('data-type', 'reg');
		}
		let form = document.getElementsByClassName('lwa-register-form');
		if (document.getElementsByName('action_type')[0]) {
			let el = document.getElementsByName('action_type')[0];
			el.remove();
		}
		let input = document.createElement('input');
		input.type = 'hidden';
		input.name = 'action_type';
		input.value = 'register';

		// console.dir(form);
		if (form[0]) {
			form[0].prepend(input);
		}
	});
}

let reg_link = document.getElementsByClassName('register-link');

if(reg_link[0] != null && reg_link[0] != undefined) {
	reg_link[0].addEventListener('click', function (e) {
		// console.log('here');
		let rep_mess = document.getElementById('report_message');
		if(rep_mess) {
			rep_mess.style.display = 'none';
			rep_mess.setAttribute('data-type', 'reg');
		}
		let form = document.getElementsByClassName('lwa-register-form');
		if (document.getElementsByName('action_type')[0]) {
			let el = document.getElementsByName('action_type')[0];
			el.remove();
		}
		let input = document.createElement('input');
		input.type = 'hidden';
		input.name = 'action_type';
		input.value = 'register';

		// console.dir(form);
		if (form[0]) {
			form[0].prepend(input);
		}
	});
}

let cycle_reg_link = document.querySelector('#cycle button.custom-btn');

if(cycle_reg_link) {
	cycle_reg_link.addEventListener('click', function (e) {
		// console.log('here');
		let rep_mess = document.getElementById('report_message');
		if(rep_mess) {
			rep_mess.style.display = 'none';
			rep_mess.setAttribute('data-type', 'reg');
		}
		let form = document.getElementsByClassName('lwa-register-form');
		if (document.getElementsByName('action_type')[0]) {
			let el = document.getElementsByName('action_type')[0];
			el.remove();
		}
		let input = document.createElement('input');
		input.type = 'hidden';
		input.name = 'action_type';
		input.value = 'register';

		// console.dir(form);
		if (form[0]) {
			form[0].prepend(input);
		}
	});
}

let promotions_reg_link = document.querySelector('#promotions button.signin');

if(promotions_reg_link) {
	promotions_reg_link.addEventListener('click', function (e) {
		// console.log('here');
		let rep_mess = document.getElementById('report_message');
		if(rep_mess) {
			rep_mess.style.display = 'none';
			rep_mess.setAttribute('data-type', 'reg');
		}
		let form = document.getElementsByClassName('lwa-register-form');
		if (document.getElementsByName('action_type')[0]) {
			let el = document.getElementsByName('action_type')[0];
			el.remove();
		}
		let input = document.createElement('input');
		input.type = 'hidden';
		input.name = 'action_type';
		input.value = 'register';

		// console.dir(form);
		if (form[0]) {
			form[0].prepend(input);
		}
	});
}

let offer_reg_link = document.querySelector('#offer button.custom-btn');
if(offer_reg_link) {
	offer_reg_link.addEventListener('click', function (e) {
		// console.log('here');
		let rep_mess = document.getElementById('report_message');
		if(rep_mess) {
			rep_mess.style.display = 'none';
			rep_mess.setAttribute('data-type', 'reg');
		}
		let form = document.getElementsByClassName('lwa-register-form');
		if (document.getElementsByName('action_type')[0]) {
			let el = document.getElementsByName('action_type')[0];
			el.remove();
		}
		let input = document.createElement('input');
		input.type = 'hidden';
		input.name = 'action_type';
		input.value = 'register';

		// console.dir(form);
		if (form[0]) {
			form[0].prepend(input);
		}
	});
}

// READ MORE - PROMOTIONS
let more = document.getElementsByClassName("more");

for (let i = 0; i < more.length; i++) {
	more[i].addEventListener('click', function (e) {
		e.preventDefault()
		console.log(e.target);

		if (e.target.localName == 'img') {
			let p_el = jQuery(e.target).siblings("p");
			let a_el = jQuery(e.target).siblings("a");
			console.log(p_el);

			console.log(p_el.scrollHeight);
			p_el.css("maxHeight", p_el.prop('scrollHeight') + 'px');
			a_el.css('display','none');
		} else if (e.target.localName == 'a') {

			this.style.display = 'none'
			let content = this.previousElementSibling;
			content.style.maxHeight = content.scrollHeight + 'px';
		}


	})
}


// READ MORE SUPER-CYCLE

const screenWidth = window.screen.width;
let textCycle = document.getElementById('textCycle');
let moreCycle = document.getElementById('moreCycle');

if (textCycle != undefined && moreCycle != undefined) {
	if (screenWidth <= 768) {
		textCycle.style.maxHeight = "200px";
		moreCycle.addEventListener('click', function (e) {
			e.preventDefault();
			textCycle.style.maxHeight = textCycle.scrollHeight + 'px';
			this.style.display = 'none';
		})
	} else {
		textCycle.style.maxHeight = (textCycle.offsetHeight / 2) + 'px'

		moreCycle.addEventListener('click', function (e) {
			e.preventDefault();
			// textCycle.style.maxHeight = textCycle.offsetHeight * 2.3 + 'px';
			textCycle.style.maxHeight = textCycle.scrollHeight  + 'px';

			this.style.display = 'none';
		})
	}
}


// MOBILE MENU CLOSE MODAL
let myBtn = document.querySelectorAll('#mobileModal a');
let myModal = document.getElementById('mobileModal');

for (let i = 0; i < myBtn.length; i++) {
	myModal.addEventListener('shown.bs.modal', function () {

		myBtn[i].addEventListener('click', clickHandler)
	})
	myModal.addEventListener('hidden.bs.modal', function () {

		myBtn[i].removeEventListener('click', clickHandler)
	})

	function clickHandler(e) {
		e.preventDefault();
		Modal.getInstance(myModal).hide();
		setTimeout(() => {
			myBtn[i].click()
		}, 500)
	}
}

// SWIPER
Swiper.use([Navigation, Pagination, Autoplay]);
const swiper = new Swiper('.swiper-container', {
	loop: true,
	navigation: {
		nextEl: '.swiper-button-next',
		prevEl: '.swiper-button-prev',
	},
	pagination: {
		el: '.swiper-pagination',
		type: 'bullets',
		clickable: true
	},
	autoplay: {
		delay: 5000,
	},
	breakpoints: {
		375: {

			slidesPerView: 1,
		},
		1024: {
			slidesPerView: 2,
		}
	},
});

// SHOW POPUP MESSAGE (Toasts) WHEN USER CONFIRM E-MAIL

window.onload = function() {
	let toastMessage = document.getElementById('ToastMessage');
 	if(toastMessage) {
		let toast = new Toast(toastMessage);
		toast.show();
	}
}

// ADD LINK ON SITE WHEN USER COPY TEXT
function addLink() {
	let body_element = document.getElementsByTagName('body')[0];
	let selection;
	selection = window.getSelection();
	let pagelink = "<br /><br /> Подробнее на: <a href='" + document.location.href+"'>" + document.location.href + "</a><br />";
	let copytext = selection + pagelink;
	let newdiv = document.createElement('div');
	newdiv.style.position='absolute';
	newdiv.style.left='-99999px';
	body_element.appendChild(newdiv);
	newdiv.innerHTML = copytext;
	selection.selectAllChildren(newdiv);
	window.setTimeout(function() {
		body_element.removeChild(newdiv);
	}, 0);
}
document.oncopy = addLink;




// SHOW MODAL DIALOG WHEN USER CLOSE TAB OR WINDOW

// window.addEventListener("beforeunload", function (event) {
// 	event.preventDefault();
// 	event.returnValue = 'sdklfgjlskdfgjklsdfjg gsdflgksdflg!';
// });

// $(document).mouseleave(function(){alert('123');
// 	console.log('123')});

// console.log(window.location.href);

jQuery(document).ready(function(){
	jQuery(".footer div.button").click(function (){
		window.open('https://t.me/gold_bull_invest');
	});
	jQuery(".modal-body-content div.button").click(function (){
		window.open('https://t.me/gold_bull_invest');
	});
	//add hide|show action
	jQuery(".pwd-log .hide-pw-btn").click((e)=>{
		if( jQuery(".pwd-log .hide-pw-btn").attr("data-togle") == "0" ) {
			jQuery(".pwd-log .hide-pw-btn").html("<i class=\"fas fa-eye-slash\"></i>");
			jQuery(".pwd-log .hide-pw-btn").attr("data-togle", '1');
			jQuery(".pwd-log > input").attr("type", "text");
		} else if (jQuery(".pwd-log .hide-pw-btn").attr("data-togle") == "1") {
			jQuery(".pwd-log .hide-pw-btn").html("<i class=\"fas fa-eye\"></i>");
			jQuery(".pwd-log .hide-pw-btn").attr("data-togle", '0');
			jQuery(".pwd-log > input").attr("type", "password");
		}
	});

	jQuery(".pwd-reg-1 .hide-pw-btn").click((e)=>{
		if( jQuery(".pwd-reg-1 .hide-pw-btn").attr("data-togle") == "0" ) {
			jQuery(".pwd-reg-1 .hide-pw-btn").html("<i class=\"fas fa-eye-slash\"></i>");
			jQuery(".pwd-reg-1 .hide-pw-btn").attr("data-togle", '1');
			jQuery(".pwd-reg-1 > input").attr("type", "text");
		} else if (jQuery(".pwd-reg-1 .hide-pw-btn").attr("data-togle") == "1") {
			jQuery(".pwd-reg-1 .hide-pw-btn").html("<i class=\"fas fa-eye\"></i>");
			jQuery(".pwd-reg-1 .hide-pw-btn").attr("data-togle", '0');
			jQuery(".pwd-reg-1 > input").attr("type", "password");
		}
	});

	jQuery(".pwd-reg-2 .hide-pw-btn").click((e)=>{
		if( jQuery(".pwd-reg-2 .hide-pw-btn").attr("data-togle") == "0" ) {
			jQuery(".pwd-reg-2 .hide-pw-btn").html("<i class=\"fas fa-eye-slash\"></i>");
			jQuery(".pwd-reg-2 .hide-pw-btn").attr("data-togle", '1');
			jQuery(".pwd-reg-2 > input").attr("type", "text");
		} else if (jQuery(".pwd-reg-2 .hide-pw-btn").attr("data-togle") == "1") {
			jQuery(".pwd-reg-2 .hide-pw-btn").html("<i class=\"fas fa-eye\"></i>");
			jQuery(".pwd-reg-2 .hide-pw-btn").attr("data-togle", '0');
			jQuery(".pwd-reg-2 > input").attr("type", "password");
		}
	});
});